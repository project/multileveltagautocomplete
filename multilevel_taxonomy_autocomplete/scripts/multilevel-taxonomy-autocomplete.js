/**
 * @file
 * Multilevel Taxonomy Autocomplete js file.
 */

var $ = jQuery;
(function ($) {
  Drupal.multilevel_taxonomy_autocomplete = Drupal.multilevel_taxonomy_autocomplete || {};
  /**
   * Attaching events to elements.
   */
  Drupal.behaviors.multilevel_taxonomy_autocomplete = {
    attach: function(context) {
      prepopulateTokens = null;
      $('input[type=text]').each(function () {
        var taxonomy_attr = $(this).attr('multi_level_taxonomy_auto_complete');
        if (typeof taxonomy_attr !== typeof undefined && taxonomy_attr !== false) {
          var term_reference_value = $(this).attr('taxonomy_term_reference');
          var inputid = $(this).attr('id');
          var tid = $("#" + inputid).val();
          if (tid != '') {
            prepopulateTokens = multilevel_taxonomy_autocomplete_get_switch_prepopulate_tokens(tid);
          }
          if ($('.token-input-list').length == 0) {
            if (prepopulateTokens) {
              prepopulateTokens = prepopulateTokens;
            }
            else {
              prepopulateTokens = null;
            }
            availabileTokens = multilevel_taxonomy_autocomplete_get_available_tokens(term_reference_value);
            $("#" + inputid).tokenInput(availabileTokens, {
              prePopulate: prepopulateTokens
            })
          }
        }
      });
    }
  };

  /**
   * Function to get the taxonomy list by passing the vocabulary machine name.
   */
  function multilevel_taxonomy_autocomplete_get_available_tokens(term_reference_value) {
    var availabileTokens = null;
    $.ajax({
      type : 'POST',
      url : '/ajax/category/autocomplete',
      async: false,
      data: {
        term_reference_value: term_reference_value,
      },
      success: function(data) {
        availabileTokens = $.parseJSON(data);
      }
    });
    return availabileTokens;
  }

  /**
   * Function to get the taxonomy names by passing the term ids.
   */
  function multilevel_taxonomy_autocomplete_get_switch_prepopulate_tokens(tid) {
    var prepopulateTokens = null;
    if (tid) {
      $.ajax({
        type : 'POST',
        async: false,
        data: {
          id: tid,
        },
        url : '/ajax/get/switch-category',
        success: function(data) {
          prepopulateTokens = $.parseJSON(data);
        }
      });
    }
    return prepopulateTokens;
  }

})(jQuery);
