This module gives a flat taxonomy with heirarchy
as autocomplete suggestion for a text field in custom form.
This module uses the jquery token input library
for the autocomplete functionality.

For using this module.
----------------------
  1. Download and the jquery.tokeninput library from
      https://github.com/loopj/jquery-tokeninput/tree/jquery-tokeninput-1.6.0
  2. Extract it and rename the folder to 'jquery-tokeninput' and add it
     to the libraries folder.
  3. Enable the module.
  4. Generate the flat hierarchies using the 'Generate Flat Hierarchy'
     button for the corresponding vocabularies.
     The button can be found in the edit page of the vocabulary.
     Only by doing this you will get the terms in dropdown.
  5. In custom form for textfield that requires autocomplete
     add the following as attributes.
      a. multi_level_taxonomy_auto_complete => TRUE.
      b. taxonomy_term_reference => < the machine-name of the vocabulary >.
      c. In edit form provide the comma separated taxonomy term ids as
         default_value.
      Example form element looks like
      $form['categories'] = array(
        '#type' => 'textfield',
        '#title' => t('Categories'),
        '#default_value' => '6,2', // Comma separated term ids.
        '#attributes' => array(
          'id' => 'test-categories',
          // Specifies this fields needs autocomplete.
          'multi_level_taxonomy_auto_complete' => TRUE,
          // The machine name of the vocabulary.
          'taxonomy_term_reference' => 'test_category',
        ),
      );
  6. You can change the look and feel of the dropdown and selected
     items by overriding the css.
